package main.java.com.medical.platform.model;

import antlr.collections.impl.LList;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Pacient{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String gen;
    private Date date;
    private String rol;
    private String address;
    private String username;
    private String password;
    @JsonIgnore
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="idAsistent")
    private Asistent asistent;
    private String medRec;
    @OneToMany
    @JoinColumn(name="idPacient")
    private List<PMedication> medPlan= new ArrayList<>();


    public Pacient(){

    }

    public Pacient(String name, String gen, Date date, String rol, String address, String username, String password, Asistent asistent, String medRec, List<PMedication> medPlan) {
        this.name = name;
        this.gen = gen;
        this.date = date;
        this.rol = rol;
        this.address = address;
        this.username = username;
        this.password = password;
        this.asistent = asistent;
        this.medRec = medRec;
        this.medPlan = medPlan;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public Date  getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Asistent getAsistent() {
        return asistent;
    }

    public void setAsistent(Asistent asistent) {
        this.asistent = asistent;
    }

    public String getMedRec() {
        return medRec;
    }

    public void setMedRec(String medRec) {
        this.medRec = medRec;
    }

    public List<PMedication> getMedPlan() {
        return medPlan;
    }

    public void setMedPlan(List<PMedication> medPlan) {
        this.medPlan = medPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pacient)) return false;
        Pacient pacient = (Pacient) o;
        return Objects.equals(getId(), pacient.getId()) &&
                Objects.equals(getName(), pacient.getName()) &&
                Objects.equals(getGen(), pacient.getGen()) &&
                Objects.equals(getDate(), pacient.getDate()) &&
                Objects.equals(getRol(), pacient.getRol()) &&
                Objects.equals(getAddress(), pacient.getAddress()) &&
                Objects.equals(getUsername(), pacient.getUsername()) &&
                Objects.equals(getPassword(), pacient.getPassword()) &&
                Objects.equals(getAsistent(), pacient.getAsistent()) &&
                Objects.equals(getMedRec(), pacient.getMedRec()) &&
                Objects.equals(getMedPlan(), pacient.getMedPlan());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getGen(), getDate(), getRol(), getAddress(), getUsername(), getPassword(), getAsistent(), getMedRec(), getMedPlan());
    }

    @Override
    public String toString() {
        return "Pacient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gen='" + gen + '\'' +
                ", date=" + date +
                ", rol='" + rol + '\'' +
                ", address='" + address + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", asistent=" + asistent +
                ", medRec='" + medRec + '\'' +
                ", medPlan=" + medPlan +
                '}';
    }


}
