package main.java.com.medical.platform.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Entity
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int doza;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name="idMedication")
    private List<Plan> plan;
    public Medication() {
    }
    public Medication(String name, int doza) {
        this.name = name;
        this.doza = doza;

    }
    public void setId(int id) {
        this.id = id;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getDoza() {
        return doza;
    }

    public List<Plan> getPlan() {
        return plan;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDoza(int doza) {
        this.doza = doza;
    }


    @Override
    public String toString() {
        return "Medication{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", doza=" + doza +
                '}';
    }
}
