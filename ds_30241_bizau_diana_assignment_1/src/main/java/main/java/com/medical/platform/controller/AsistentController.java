package main.java.com.medical.platform.controller;

import main.java.com.medical.platform.model.Asistent;
import main.java.com.medical.platform.repos.AsistentRepo;
import main.java.com.medical.platform.repos.PacientRepo;
import main.java.com.medical.platform.service.AsistentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PostUpdate;
import java.util.List;

@RestController
@RequestMapping("/Asistent")
public class AsistentController {
    @Autowired
    private AsistentService asistentService;

    @Autowired
    private AsistentRepo asistentRepo;

    @Autowired
    private PacientRepo pacientRepo;
    @GetMapping("/getAsistenti")
    public List<Asistent> getAsistenti(Asistent asistent){
        return asistentService.findAll();
    }
    @DeleteMapping("/deleteAsistent/{id}")
    public void deleteAsistenti(Long id){
        asistentService.delete(id);
    }
    @PutMapping("/updateAsistent")
    public Asistent update(Asistent asistent){
        return asistentService.update(asistent);
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity<?> deleteAsistent(Long id)
    {
        asistentRepo.delete(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/remove/{id}")
    ResponseEntity<?> deletePacient(Long id)
    {
        pacientRepo.delete(id);
        return ResponseEntity.ok().build();
    }
}
