package main.java.com.medical.platform.dao;

import main.java.com.medical.platform.model.Asistent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AsistentDAO extends JpaRepository<Asistent,Integer> {
}
