package main.java.com.medical.platform.model;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
@Entity
public class PMedication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @JsonIgnore
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="idPacient")
    private Pacient pacient;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="idDoctor")
    private Doctor doctor;
    @OneToMany
    @JoinColumn(name="idPMedication")
    private List<Plan> plan=new ArrayList<>();
    public PMedication(){}

    public PMedication(Long id, Pacient pacient, Doctor doctor, List<Plan> plan) {
        this.id = id;
        this.pacient = pacient;
        this.doctor = doctor;
        this.plan = plan;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pacient getPacient() {
        return pacient;
    }

    public void setPacient(Pacient pacient) {
        this.pacient = pacient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public List<Plan> getPlan() {
        return plan;
    }

    public void setPlan(List<Plan> plan) {
        this.plan = plan;
    }

    @Override
    public String toString() {
        return "PMedication{" +
                "id=" + id +
                ", pacient=" + pacient +
                ", doctor=" + doctor +
                ", plan=" + plan +
                '}';
    }
}
