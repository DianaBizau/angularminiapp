package main.java.com.medical.platform.controller;

import main.java.com.medical.platform.model.Pacient;
import main.java.com.medical.platform.service.PacientService;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/App")
public class PacientController {
    @Autowired
    private PacientService pacientService;
    @GetMapping("/getPacienti")
    public List<Pacient> getPacienti(Pacient pacient)
    {
        return pacientService.findAll();
    }
    @PutMapping("/updatePacienti")
    public Pacient updatePacienti(Pacient pacient)
    {
        return pacientService.updatePacient(pacient);
    }
    @DeleteMapping("/deletePacienti/{id}")
    public void deletePacienti(Long id){
        pacientService.delete(id);
    }
    @PostMapping("/createPacienti")
    public Pacient createPacient(Pacient pacient){
        return pacientService.createPacient(pacient);
    }
}
