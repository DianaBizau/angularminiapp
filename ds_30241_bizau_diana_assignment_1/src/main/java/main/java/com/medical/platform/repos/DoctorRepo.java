package main.java.com.medical.platform.repos;

import main.java.com.medical.platform.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface DoctorRepo extends CrudRepository<Doctor,Integer> {
    Doctor findByUsername(String username);
}
