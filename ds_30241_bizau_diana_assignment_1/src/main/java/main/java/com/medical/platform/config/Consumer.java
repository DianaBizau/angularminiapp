package main.java.com.medical.platform.config;

import main.java.com.medical.platform.model.Anomaly;
import main.java.com.medical.platform.model.Asistent;
import main.java.com.medical.platform.model.Pacient;
import main.java.com.medical.platform.service.AnomalyService;
import main.java.com.medical.platform.service.AsistentService;
import main.java.com.medical.platform.service.PacientService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

@Component
public class Consumer {

    private String message;
    private PacientService pacientService;
    private AsistentService asistentService;
    private AnomalyService anomalyService;

    @Autowired
    private Consumer(PacientService pacientService, AsistentService asistentService, AnomalyService anomalyService) {
        this.pacientService = pacientService;
        this.asistentService = asistentService;
        this.anomalyService = anomalyService;
    }

    @RabbitListener(queues = "${jsa.rabbitmq.queue}")
    public void recievedMessage(String msg) throws InterruptedException, ParseException {
        String[] splits = msg.split("\\t+");
        DateTimeFormatter ft = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        if(splits.length>2) {
            LocalDateTime start = LocalDateTime.parse(splits[1], ft);
            LocalDateTime end = LocalDateTime.parse(splits[2], ft);
            long hours = ChronoUnit.HOURS.between(start, end);
            //R1
            if (splits[3].equals("Sleeping") && hours >= 12) {
                Pacient patient = pacientService.findById(Long.parseLong(splits[0]));
                Asistent caregiver = asistentService.findById(patient.getAsistent().getId());
                Anomaly anomaly = new Anomaly(caregiver.getId(), patient.getId(), "R1");
                anomalyService.save(anomaly);
                System.out.println(anomaly.toString());
            } else {
                //R2
                if (splits[3].equals("Leaving") && hours >= 12) {
                    Pacient patient = pacientService.findById(Long.parseLong(splits[0]));
                    Asistent caregiver = asistentService.findById(patient.getAsistent().getId());
                    Anomaly anomaly = new Anomaly(caregiver.getId(), patient.getId(), "R2");
                    anomalyService.save(anomaly);
                    System.out.println(anomaly.toString());
                } else {
                    //R3
                    if ((splits[3].equals("Toileting") || splits[2].equals("Showering") || splits[2].equals("Grooming")) && hours >= 1) {
                        Pacient patient = pacientService.findById(Long.parseLong(splits[0]));
                        Asistent caregiver = asistentService.findById(patient.getAsistent().getId());
                        Anomaly anomaly = new Anomaly(caregiver.getId(), patient.getId(), "R3");
                        anomalyService.save(anomaly);
                        System.out.println(anomaly.toString());
                    } else {
                        System.out.println("Recieved Message: " + msg);
                    }
                }
            }
        }
    }
}