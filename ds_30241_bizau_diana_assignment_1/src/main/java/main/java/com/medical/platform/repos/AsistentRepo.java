package main.java.com.medical.platform.repos;

import main.java.com.medical.platform.model.Asistent;
import org.springframework.data.repository.CrudRepository;

public interface AsistentRepo extends CrudRepository<Asistent,Long> {

    public Asistent findById(Long id);
}
