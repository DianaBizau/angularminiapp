package main.java.com.medical.platform.dao;

import main.java.com.medical.platform.model.Pacient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PacientDAO extends JpaRepository<Pacient,Long> {
}
