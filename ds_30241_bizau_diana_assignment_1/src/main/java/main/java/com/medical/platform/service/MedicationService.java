package main.java.com.medical.platform.service;

import main.java.com.medical.platform.model.Medication;
import main.java.com.medical.platform.repos.MedicationRepo;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicationService {
    @Autowired
    private MedicationRepo medicationRepo;

    public Medication save (String name, int doza){
        Medication med=new Medication(name,doza);
        return medicationRepo.save(med);
    }
    public List<Medication> findAll(){

        return Lists.newArrayList(medicationRepo.findAll());
    }
}
