package main.java.com.medical.platform.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Plan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String period;
    private String ore;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="idMedication")
    private Medication medication;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="idPMedication")
    private PMedication planMed;

    public Plan()
    {

    }

    public Plan(Long id, String period, String ore, Medication medication, PMedication planMed) {
        this.id = id;
        this.period = period;
        this.ore = ore;
        this.medication = medication;
        this.planMed = planMed;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getOre() {
        return ore;
    }

    public void setOre(String ore) {
        this.ore = ore;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public PMedication getPlanMed() {
        return planMed;
    }

    public void setPlanMed(PMedication planMed) {
        this.planMed = planMed;
    }

    @Override
    public String toString() {
        return "Plan{" +
                "id=" + id +
                ", period='" + period + '\'' +
                ", ore='" + ore + '\'' +
                ", medication=" + medication +
                ", planMed=" + planMed +
                '}';
    }
}
