package main.java.com.medical.platform.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.com.medical.platform.model.*;
import main.java.com.medical.platform.repos.DoctorRepo;
import main.java.com.medical.platform.repos.MedicationRepo;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.security.Principal;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
public class DoctorService {
    @Autowired
    private DoctorRepo doctorRepo;
    @Autowired
    private PacientService pacientService;
    @Autowired
    private AsistentService asistentService;

    private MedicationRepo medicationRepo;

    public List<Doctor> findAll() {
        return Lists.newArrayList(doctorRepo.findAll());
    }

    public Doctor save(String name, LocalDateTime date, String gen, String addr, String username, String password) {
        Doctor doctor = doctorRepo.save(new Doctor(name,gen,date,null,addr,username,password,null));
        return doctor;
    }

    public Pacient addPacient(String name, Date date, String gen, String addr, String username, String password, String medicalRecord) {
        Pacient pacient = pacientService.addPacient(name,gen,date,null,addr,username,password,medicalRecord);
        return pacient;
    }

    public Asistent addAsistent(String name,String date, String gen,String addr,String username,String password){
        Asistent asistent=asistentService.save(name,date,gen,addr,username,password);
        return asistent;
    }
    public void deleteAsistent(Long id){
        asistentService.delete(id);
    }

    public void deletePacient(Long id){
        pacientService.delete(id);
    }

    public void addPatientToCaregiver(Long patientId, Long caregiverId) {
        Pacient p = pacientService.findById(patientId);
        Asistent c= asistentService.findById(caregiverId);
        p.setAsistent(c);
        c.addPacient(p);
        asistentService.save(c);
        pacientService.save(p);
        System.out.println(pacientService.findById(patientId));
    }

    public void addMedicationToPatient(Doctor d, Pacient p, Medication m, String intervalHours, String period) {
        p = pacientService.findById(p.getId());
        Plan mmPlan = new Plan();
        mmPlan.setOre(intervalHours);
        mmPlan.setPeriod(period);
        mmPlan.setMedication(m);
        PMedication medicationPlan = new PMedication();
        medicationPlan.setDoctor(d);
        //medicationPlan.setPacient();
       // medicationPlan = medicationRepo.save(mmPlan);


       // p.addMedicationPlan(medicationPlan);
       // pacientService.save(p);

        //mmPlan.setMedicationPlan(medicationPlan);

        //mmPlanRepository.save(mmPlan);

    }


}
