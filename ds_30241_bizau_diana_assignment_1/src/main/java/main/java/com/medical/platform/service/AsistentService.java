package main.java.com.medical.platform.service;

import main.java.com.medical.platform.model.Asistent;
import main.java.com.medical.platform.repos.AsistentRepo;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AsistentService {
    @Autowired
    private AsistentRepo asistentRepo;


    public Asistent findById(Long id) {
        return asistentRepo.findById(id);
    }

    public Asistent save (String name, String date,String gen,String addr,String username,String password){
        Asistent asistent=new Asistent(name,gen,date,null,addr,username,password,null);
        return asistentRepo.save(asistent);
    }
    public Asistent save (Asistent asistent){
        return asistentRepo.save(asistent);
    }
   public List<Asistent> findAll(){

        return Lists.newArrayList(asistentRepo.findAll());
   }
   public void delete(Long id){
        asistentRepo.delete(id);
   }
   public Asistent update(Asistent asistent){
        return asistentRepo.save(asistent);
   }
}
