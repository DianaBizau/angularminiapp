package main.java.com.medical.platform.dao;

import main.java.com.medical.platform.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorDAO extends JpaRepository<Doctor,Integer> {
}
