package main.java.com.medical.platform.repos;

import main.java.com.medical.platform.model.PMedication;
import org.springframework.data.repository.CrudRepository;

public interface PMedicationRepo extends CrudRepository<PMedication,Integer> {
}
