package main.java.com.medical.platform.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Anomaly {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private Long id;

    @JoinColumn(name = "patient_id")
    private Long pacientId;


    @JoinColumn(name = "asistent_id")
    private Long asistentId;
    private String anomaly;

    public Anomaly(Long asistentId, Long pacientId, String anomaly) {
        this.pacientId = pacientId;
        this.asistentId = asistentId;
        this.anomaly = anomaly;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPacientId() {
        return pacientId;
    }

    public void setPacientId(Long pacientId) {
        this.pacientId = pacientId;
    }

    public Long getAsistentId() {
        return asistentId;
    }

    public void setAsistentId(Long asistentId) {
        this.asistentId = asistentId;
    }

    public String getAnomaly() {
        return anomaly;
    }

    public void setAnomaly(String anomaly) {
        this.anomaly = anomaly;
    }

    @Override
    public String toString() {
        return "Anomaly{" +
                "id=" + id +
                ", pacientId=" + pacientId +
                ", asistentId=" + asistentId +
                ", anomaly='" + anomaly + '\'' +
                '}';
    }
}
