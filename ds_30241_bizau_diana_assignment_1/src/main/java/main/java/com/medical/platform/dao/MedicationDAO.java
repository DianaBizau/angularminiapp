package main.java.com.medical.platform.dao;

import main.java.com.medical.platform.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationDAO extends JpaRepository<Medication,Integer> {
}
