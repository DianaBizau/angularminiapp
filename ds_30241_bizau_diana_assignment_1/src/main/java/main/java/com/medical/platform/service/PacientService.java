package main.java.com.medical.platform.service;

import main.java.com.medical.platform.dao.PacientDAO;
import main.java.com.medical.platform.model.Pacient;
import main.java.com.medical.platform.repos.PacientRepo;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import static java.lang.Integer.valueOf;

@Service
public class PacientService {
    @Autowired
    private PacientRepo pacientRepo;
    private PacientDAO pacientDao;

    public Pacient addPacient (String name, String gen, Date date, String rol, String addr, String username, String password, String medicRec)
    {
        Pacient pacient=new Pacient(name,gen,date,rol,addr,username,password,null,medicRec,null);
        return pacientRepo.save(pacient);
    }
    public Pacient save(Pacient p) {
        return pacientRepo.save(p);
    }

    public List<Pacient> findAll() {
        return Lists.newArrayList(pacientRepo.findAll());
    }

    public Pacient updatePacient(Pacient pacient)
    {
        return pacientRepo.save(pacient);
    }
    public Pacient findById(Long id) {
        return pacientRepo.findById(id);
    }
    public void delete(Long id) {
        pacientDao.delete(id);
    }
    public Pacient createPacient(Pacient pacient){
        return pacientDao.save(pacient);
    }
}
